import { BehaviorSubject, fromEvent, Subject } from 'rxjs';
import { first, map, share, take } from 'rxjs/operators';
import {
  decodeStatusResponse,
  encodeCommand,
  getStartResistCommand,
  observableCharacteristic,
} from './lib/brainbit-utils';
import {
  BLE_UUID_NSS2_COMMAND_CHAR,
  BLE_UUID_NSS2_STATUS_CHAR,
  BRAINBIT_SERVICE,
  commands,
  DEVICE_INFO_CHARACTERISTICS_UUIDS_BY_NAME,
  DEVICE_INFORMATION_SERVICE, DEVICE_MODE,
} from './lib/static';
import BrainbitSubscribes from './lib/brainbit-subscribes';

class BrainbitClient {
  constructor() {
    this.gatt = null;
    this.eegStream = null;
    this.eventMarkers = null;
    this.connectionStatus = new BehaviorSubject(false);
    this.statusData = null;
    this.deviceInfoObject = null;
    this.resistanceData = null;
    this.brainbitSubscribes = null;

    this.sendCommand = this.sendCommand.bind(this);
  }

  async connect() {
    const device = await navigator.bluetooth.requestDevice({
      filters: [{ name: 'BrainBit' }],
      optionalServices: [BRAINBIT_SERVICE, DEVICE_INFORMATION_SERVICE],
    });
    this.gatt = await device.gatt.connect();

    const service = await this.gatt.getPrimaryService(BRAINBIT_SERVICE);
    fromEvent(this.gatt.device, 'gattserverdisconnected')
    .pipe(first())
    .subscribe(() => {
      this.gatt = null;
      this.connectionStatus.next(false);
    });

    this.statusChar = await service.getCharacteristic(BLE_UUID_NSS2_STATUS_CHAR);
    this.statusData = (await observableCharacteristic(this.statusChar)).pipe(
      map((data) => decodeStatusResponse(new Uint8Array(data.buffer))),
      share(),
    );
    this.brainbitSubscribes = new BrainbitSubscribes(service, this.sendCommand, this.statusData);
    this.commandChar = await service.getCharacteristic(BLE_UUID_NSS2_COMMAND_CHAR);

    this.eventMarkers = new Subject();
    this.resistanceData = await this.brainbitSubscribes.getResistanceData();
    this.eegStream = await this.brainbitSubscribes.getEegStream();
    this.connectionStatus.next(true);
  }

  async sendCommand(cmd) {
    this.checkGatt();
    await this.commandChar.writeValue(encodeCommand(cmd));
  }

  async checkStatus() {
    this.checkGatt();
    const dataView = await this.statusChar.readValue();
    return decodeStatusResponse(new Uint8Array(dataView.buffer));
  }

  async startEEGStream() {
    this.checkGatt();
    const statusData = await this.checkStatus();
    const resultListener = this.statusData.pipe(map((r) => r.status), take(1)).toPromise();
    if (statusData.status.name === 'NSS2_STATUS_STOPED'
      || statusData.status.name === 'NSS2_STATUS_SIGNAL'
      || statusData.status.name === 'NSS2_STATUS_RESIST') {
      await this.sendCommand(commands.signal);
      this.brainbitSubscribes.setMode(DEVICE_MODE.SIGNAL);
    } else if (statusData.status.name === 'NSS2_STATUS_BOOTLOADER_NEED') {
      throw new Error('Cannot start EEG stream. Bootloader is needed');
    } else if (statusData.status.name === 'NSS2_STATUS_INVALID') {
      throw new Error('Cannot start EEG stream. Service is not initialized');
    }
    return resultListener;
  }

  async stopEEGStream() {
    this.checkGatt();
    const resultListener = this.statusData.pipe(map((r) => r.status), take(1)).toPromise();
    await this.sendCommand(commands.stop);
    return resultListener;
  }

  async stopResistanceData() {
    this.checkGatt();
    const resultListener = this.statusData.pipe(map((r) => r.status), take(1)).toPromise();
    await this.sendCommand(commands.stop);
    return resultListener;
  }

  async startResistanceData() {
    this.checkGatt();
    const statusData = await this.checkStatus();
    const resultListener = this.statusData.pipe(map((r) => r.status), take(1)).toPromise();

    if (statusData.status.name === 'NSS2_STATUS_STOPED'
      || statusData.status.name === 'NSS2_STATUS_SIGNAL'
      || statusData.status.name === 'NSS2_STATUS_RESIST') {
      await this.sendCommand(getStartResistCommand(1, false));
      this.brainbitSubscribes.setMode(DEVICE_MODE.RESISTANCE);
    } else if (statusData.status.name === 'NSS2_STATUS_BOOTLOADER_NEED') {
      throw new Error('Cannot start EEG stream. Bootloader is needed');
    } else if (statusData.status.name === 'NSS2_STATUS_INVALID') {
      throw new Error('Cannot start EEG stream. Service is not initialized');
    }
    return resultListener;
  }

  async deviceInfo() {
    if (!this.gatt) {
      return {
        state: 'DISCONNECTED',
        name: 'BrainBit',
      };
    }
    if (!this.deviceInfoObject) {
      const deviceInformationService = await this.gatt.getPrimaryService(DEVICE_INFORMATION_SERVICE);
      const characteristics = await deviceInformationService.getCharacteristics();
      const decoder = new TextDecoder('utf-8');
      this.deviceInfoObject = {};
      this.deviceInfoObject.model = decoder.decode(await characteristics
      .find((c) => c.uuid === DEVICE_INFO_CHARACTERISTICS_UUIDS_BY_NAME.model)
      .readValue());
      this.deviceInfoObject.hardwareRevision = decoder.decode(await characteristics
      .find((c) => c.uuid === DEVICE_INFO_CHARACTERISTICS_UUIDS_BY_NAME.hardwareRevision)
      .readValue());
      this.deviceInfoObject.firmwareVersion = decoder.decode(await characteristics
      .find((c) => c.uuid === DEVICE_INFO_CHARACTERISTICS_UUIDS_BY_NAME.firmwareVersion)
      .readValue());
    }
    this.deviceInfoObject.name = this.gatt.device.name;
    this.deviceInfoObject.deviceId = this.gatt.device.id;
    this.deviceInfoObject.state = (this.gatt.device.gatt.connected) ? 'CONNECTED' : 'DISCONNECTED';
    return new Promise((resolve) => {
      resolve(this.deviceInfoObject);
    });
  }

  async injectMarker(value, timestamp = Date.now()) {
    await this.eventMarkers.next({ value, timestamp });
  }

  disconnect() {
    this.checkGatt();
    this.gatt.disconnect();
    this.connectionStatus.next(false);
  }

  checkGatt() {
    if (!this.gatt) {
      throw new Error('Device is not connected.');
    }
  }
}

export default BrainbitClient;
