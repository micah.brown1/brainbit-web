var path = require('path');
const HappyPack = require('happypack');

var BUILD_DIR = path.resolve(__dirname, 'dist');
var APP_DIR = path.resolve(__dirname, 'src');

var config = {
  mode: 'development',
  entry: {
    brainbitClient: APP_DIR + '/brainbit.js',
  },
  output: {
    library: 'BrainbitClient',
    path: BUILD_DIR,
    filename: '[name].umd.js',
    libraryExport: 'default',
    libraryTarget: 'umd'
  },
  plugins: [
    new HappyPack({
      loaders: [
        // Capture Babel loader
        'babel-loader',
      ],
      threads: 16,
    }),
  ],
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.(js)$/,
        exclude: [/node_modules/, /demo/],
        loader: 'eslint-loader',
        options: {
          configFile: path.join(__dirname, '.eslintrc')
        }
      },
    ]
  },
};

module.exports = config;
